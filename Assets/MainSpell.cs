﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainSpell : MonoBehaviour
{

    public GameObject target;
    public CharacterStats stats;
    public int damage;
    
    void Start()
    {
        
    }
    
    void Update()
    {
        if (target != null)
        {
            transform.LookAt(new Vector3(target.transform.position.x, target.transform.position.y + 1f, target.transform.position.z));

            float distance = Vector3.Distance(new Vector3(target.transform.position.x, target.transform.position.y + 1f, target.transform.position.z), transform.position);

            if (distance > 0.1f)
            {
                transform.Translate(Vector3.forward * 15f * Time.deltaTime); 
            }
            else {
                HitTarget();
            }
        }
    }


    public void HitTarget ()
    {
        target.GetComponent<CharacterStats>().TakeDamage(damage);
        Destroy (this.gameObject);
    }
        
}
