﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemTextUI : MonoBehaviour
{

    public GameObject uiPrefab;
    public Transform target;
    public Item item;

    Transform ui;
    Text itemText;
    Transform cam;

    void Start()
    {
        cam = Camera.main.transform;
        foreach (Canvas c in FindObjectsOfType<Canvas>())
        {
            if (c.renderMode == RenderMode.WorldSpace)
            {
                itemText = uiPrefab.GetComponentInChildren<Text>();
                itemText.text = item.name;
                ui = Instantiate(uiPrefab, c.transform).transform;
                ui.gameObject.SetActive(false);
                break;
            }
        }
    }

    private void LateUpdate()
    {
        ui.position = target.position;
        ui.forward = cam.forward;
    }

    private void OnMouseOver()
    {
        ui.gameObject.SetActive(true);
    }

    private void OnMouseExit()
    {
        ui.gameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        Destroy(ui.gameObject);
    }
}
