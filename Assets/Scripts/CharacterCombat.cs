﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CharacterStats))]
public class CharacterCombat : MonoBehaviour
{

    public float attackSpeed = 1f;
    private float attackCooldown = 0f;

    public float attackDelay = 0.6f;
    public GameObject MainAttackPrefab;
    public Skill[] skills = new Skill[2];
    public Image[] skillsUi = new Image[2];
    public int damage;

    public GameObject point;

    public event System.Action OnAttack;

    CharacterStats myStats;

    private void Start()
    {
        myStats = GetComponent<CharacterStats>();
    }

    private void Update()
    {
        skills[0].castCooldownLeft -= Time.deltaTime;
        skills[1].castCooldownLeft -= Time.deltaTime;
        Mathf.Clamp(skillsUi[0].GetComponent<Image>().fillAmount = 1 - (skills[0].castCooldownLeft / skills[0].castCooldown), 1, 0);
        Mathf.Clamp(skillsUi[1].GetComponent<Image>().fillAmount = 1 - (skills[1].castCooldownLeft / skills[1].castCooldown), 1, 0);
    }

    public void Attack(CharacterStats targetStats, Vector3 target)
    {
        int usingSkill = GetComponent<PlayerController>().usingSkill;
        if (skills[usingSkill].castCooldownLeft <= 0)
        {
            StartCoroutine(DoDamage(targetStats, skills[usingSkill].castDelay, target, usingSkill));
            
            if (OnAttack != null)
            {
                OnAttack();
            }

            skills[usingSkill].castCooldownLeft = skills[usingSkill].castCooldown;
        }
    }

    IEnumerator DoDamage(CharacterStats stats, float delay, Vector3 target, int usingSkill)
    {
        yield return new WaitForSeconds(delay);
        skillsUi[usingSkill].GetComponent<Image>().fillAmount = 0;
        skills[usingSkill].Cast(myStats.damage.baseValue);
        Vector3 SpawnSpellLoc = new Vector3(point.transform.position.x, point.transform.position.y, point.transform.position.z);
        GameObject clone;
        clone = Instantiate(skills[usingSkill].skillPrefab, SpawnSpellLoc, Quaternion.identity);
        clone.GetComponent<MainSpell>().target = stats.gameObject;
        clone.GetComponent<MainSpell>().stats = myStats;
        clone.GetComponent<MainSpell>().damage = skills[usingSkill].damageMult * skills[usingSkill].playerDamage;
    }

}

