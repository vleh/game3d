﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New TargetableSkill", menuName = "Skills/TargetableSkill")]
public class TargetableSkill : Skill
{
    public int skillDamage;

    /*void Update()
    {
        if (active)
        {
            skillPrefab.transform.LookAt(target);

            float distance = Vector3.Distance(target, skillPrefab.transform.position);

            if (distance > 0.01f)
            {
                skillPrefab.transform.Translate(Vector3.forward * 15f * Time.deltaTime);
            }
            else
            {
                HitTarget();
            }
        }
    }*/

    public override void Cast(int damage)
    {
        base.Cast(damage);
        skillDamage = playerDamage * damageMult;
    }

    public void HitTarget()
    {
        Destroy(this.skillPrefab.gameObject);
    }
}
