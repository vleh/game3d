﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Skill : ScriptableObject
{
 
    public GameObject skillPrefab;
    public int damageMult;
    public int playerDamage;

    public float castSpeed;
    public float castDelay;
    public float castCooldown;
    public float castCooldownLeft;
    
    private void Awake()
    {
        castCooldownLeft = castCooldown;
    }

    public virtual void Cast(int damage)
    {
        playerDamage = damage;
    }

}
