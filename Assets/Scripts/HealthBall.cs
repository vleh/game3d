﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBall : MonoBehaviour
{

    public GameObject player;
    public Image healthSlider;

    void Start()
    {
        player.GetComponent<CharacterStats>().OnHealthChanged += OnHealthChanged;
        //CharacterStats.OnHealthChanged += OnHealthChanged;
       
    }

    void OnHealthChanged(int maxHealth, int currentHealth)
    {
            float healthPercent = (float)currentHealth / maxHealth;
            healthSlider.fillAmount = healthPercent;
    }
}
